function createNewUser() {
    const userFirstName = prompt('Please, enter your name');
    const userLastName = prompt('Please, enter your surname');
    const birthday = prompt('Please, enter your birthday date', 'dd.mm.yyyy');
    const birthdayArr = birthday.split('.')
    const newUser = {
        firstName: userFirstName,
        lastName: userLastName,
        getLogin() {
            return (this.firstName.charAt(0) + this.lastName).toLowerCase();
        },
        getAge() {
            return (new Date() - new Date(`${birthdayArr[2]}-${birthdayArr[1]}-${birthdayArr[0]}`)) / (24 * 3600 * 362.25 * 1000) | 0;
        },
        getPassword() {
return (this.firstName.charAt(0)).toUpperCase() + (this.lastName).toLowerCase() + (`${birthdayArr[2]}`);
        }
    };
    return newUser;
}

const user = createNewUser();
console.log(user.getAge());
console.log(user.getPassword());
